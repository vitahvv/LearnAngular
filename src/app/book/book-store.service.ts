import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import {BOOKS} from './mock-books';
import {Book} from './book';


@Injectable()
export class BookStoreService {

  booksList: Book[] = BOOKS;

  getBooks(title: string): Observable<Book[]> {
    return Observable.of(this.filterBooks(title));
  }

  getBookTitles(title: string): Observable<string[]> {
    return Observable.of(this.filterBooks(title)
      .map(book => book.title));
  }

  filterBooks(title: string): Book[] {
    return title ?
      this.booksList.filter((book) => new RegExp(title, 'gi').test(book.title)) :
      [];
  }
}
