import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from "@angular/forms"
import {ReactiveFormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
// import {BookSearchComponent} from  './book/book-search/book-search.component';
import {BooksListComponent} from './book/books-list/books-list.component';
import {FormControlComponent} from './form-control/form-control.component';
import {FormGroupComponent} from './form-group/form-group.component';
import {RegistrationFormComponent} from './registration-form/registration-form.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    // BookSearchComponent,
    BooksListComponent,
    FormControlComponent,
    FormGroupComponent,
    RegistrationFormComponent
  ],
  imports: [
    BrowserModule, FormsModule, ReactiveFormsModule, NgbModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
