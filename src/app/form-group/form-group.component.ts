import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {ReactiveFormsModule} from "@angular/forms"

@Component({
  selector: 'form-group',
  templateUrl: './form-group.component.html',
  styleUrls: ['./form-group.component.css']
})
export class FormGroupComponent {
  address: FormGroup;

  constructor() {
    this.address = new FormGroup({
      street: new FormControl(''),
      city: new FormControl(''),
      state: new FormControl(''),
      country: new FormControl(''),
      zip: new FormControl('')
    });
    this.address.setValue({
      street: '1-3 Strand',
      city: 'London',
      state: 'UK',
      country: 'UK',
      zip: 'WC2N 5BW'
    });
  }



}
