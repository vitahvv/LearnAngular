import { Component } from '@angular/core';

import { Book } from './book/book';
import { BookStoreService} from './book/book-store.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  //template : `<registration-form></registration-form>`,
  styleUrls: ['./app.component.css']

})
export class AppComponent {

}
